package dev.tonyowen.jlr.ui.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import dev.tonyowen.jlr.network.model.VehicleDeets

@Composable
fun CarDetailsToggleCard(
    modifier: Modifier = Modifier,
    deets: VehicleDeets,
    onDoorsChanged: (Boolean) -> Unit,
    onWindowsChanged: (Boolean) -> Unit,
    onAlarmChanged: (Boolean) -> Unit
) {
    Card(
        modifier = modifier,
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        ),
        elevation = CardDefaults.elevatedCardElevation(
            defaultElevation = 8.dp
        )
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Doors Open", modifier = Modifier.weight(1f))
                Checkbox(checked = deets.doorsOpen, onCheckedChange = {
                    onDoorsChanged(it)
                })
            }
            Divider()
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Windows Open", modifier = Modifier.weight(1f))
                Checkbox(checked = deets.windowsOpen, onCheckedChange = {
                    onWindowsChanged(it)
                })
            }
            Divider()
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Alarm Set", modifier = Modifier.weight(1f))
                Checkbox(checked = deets.alarmSet, onCheckedChange = {
                    onAlarmChanged(it)
                })
            }
        }
    }
}