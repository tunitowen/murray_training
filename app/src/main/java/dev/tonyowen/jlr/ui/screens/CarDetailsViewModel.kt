package dev.tonyowen.jlr.ui.screens

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.tonyowen.jlr.network.JlrRepository
import dev.tonyowen.jlr.network.model.VehicleDeets
import dev.tonyowen.jlr.network.model.VehicleDetails
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException

class CarDetailsViewModel(private val repo: JlrRepository): ViewModel() {

    private val _vehicleDetails = MutableStateFlow<VehicleDeets?>(null)
    val vehicleDetails: StateFlow<VehicleDeets?> = _vehicleDetails
    fun getVehicleDetails(id: Int) {
        viewModelScope.launch {
           _vehicleDetails.value = repo.getVehicleDetails(id)
        }
    }

    fun updateVehicleDetails(deets: VehicleDeets) {
        viewModelScope.launch {
            val result = repo.updateVehicleDetails(deets)
            if (result.isSuccess) {
                _vehicleDetails.value = deets
            } else {
                Log.d("ERROR_", result.toString())
            }
        }
    }
}