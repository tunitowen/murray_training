package dev.tonyowen.jlr.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.tonyowen.jlr.network.JlrRepository
import dev.tonyowen.jlr.network.model.Vehicle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class HomeScreenViewModel(private val repo: JlrRepository): ViewModel() {

    private val _vehicles = MutableStateFlow<List<Vehicle>>(emptyList())
    val vehicles: StateFlow<List<Vehicle>> = _vehicles

    private val _error = MutableStateFlow<String?>(null)
    val error: StateFlow<String?> = _error

    fun getVehicles() {
        viewModelScope.launch {
            val result = repo.getVehicles()
            if (result.isSuccess) {
                _vehicles.value = result.getOrDefault(emptyList())
            } else {
               _error.value = "Something went wrong"
            }
        }
    }
}