package dev.tonyowen.jlr.ui.theme

import android.app.Activity
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalView
import com.composables.materialcolors.MaterialColors
import com.composables.materialcolors.get

private val lightColorScheme = lightColorScheme(
    primary = MaterialColors.Blue[500],
    secondary = MaterialColors.Green[400],
    tertiary = MaterialColors.Pink[500]
)

@Composable
fun JLRTheme(
    content: @Composable () -> Unit
) {
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = lightColorScheme.primary.toArgb()
        }
    }

    MaterialTheme(
        colorScheme = lightColorScheme,
        typography = Typography,
        content = content
    )
}