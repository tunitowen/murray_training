package dev.tonyowen.jlr.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.AsyncImage
import dev.tonyowen.jlr.R
import dev.tonyowen.jlr.network.model.Vehicle

@Composable
fun CarCard(vehicle: Vehicle, onClick: () -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(dimensionResource(id = R.dimen.cornerRadius)),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        ), elevation = CardDefaults.cardElevation(
            defaultElevation = dimensionResource(id = R.dimen.marginSmall)
        )
    ) {
        Column(modifier = Modifier.clickable {
            onClick()
        }) {
            AsyncImage(
                modifier = Modifier.aspectRatio(16 / 9f).background(Color.LightGray),
                model = vehicle.image, contentDescription = null,
                contentScale = ContentScale.Crop,
            )
            Text(
                vehicle.name,
                modifier = Modifier.padding(dimensionResource(id = R.dimen.marginLarge))
            )
        }
    }
}

@Preview
@Composable
private fun CarCardPreview() {
    CarCard(vehicle = Vehicle(1, "Sadia's Sport", "image", "range-rover-sport")) {
        
    }
}