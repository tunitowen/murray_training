package dev.tonyowen.jlr.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import dev.tonyowen.jlr.ui.composables.CarDetailsToggleCard
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CarDetailsScreen(
    navController: NavController,
    id: Int?,
    viewModel: CarDetailsViewModel = koinViewModel()
) {

    LaunchedEffect(Unit) {
        viewModel.getVehicleDetails(id ?: 0)
    }

    val vehicleDetails by viewModel.vehicleDetails.collectAsState()

    Scaffold(topBar = {
        TopAppBar(
            title = { Text(vehicleDetails?.name ?: "Loading") },
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(Icons.Default.ArrowBack, null)
                }
            },
            colors = TopAppBarDefaults.topAppBarColors(
                containerColor = MaterialTheme.colorScheme.primary,
                navigationIconContentColor = Color.White,
                titleContentColor = Color.White
            )
        )
    }) { innerPadding ->
        vehicleDetails?.let { deets ->
            Column(modifier = Modifier.padding(top = innerPadding.calculateTopPadding())) {
                AsyncImage(
                    model = vehicleDetails?.image, contentDescription = null, modifier = Modifier
                        .aspectRatio(16 / 9f)
                        .background(
                            Color.LightGray
                        ), contentScale = ContentScale.Crop
                )
                Spacer(modifier = Modifier.height(24.dp))
                CarDetailsToggleCard(
                    deets = deets,
                    modifier = Modifier.padding(horizontal = 24.dp),
                    onDoorsChanged = {
                        viewModel.updateVehicleDetails(deets.copy(doorsOpen = it))
                    },
                    onWindowsChanged = {
                        viewModel.updateVehicleDetails(deets.copy(windowsOpen = it))
                    },
                    onAlarmChanged = {
                        viewModel.updateVehicleDetails(deets.copy(alarmSet = it))
                    })
            }
        }

    }
}