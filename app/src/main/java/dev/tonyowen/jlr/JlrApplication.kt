package dev.tonyowen.jlr

import android.app.Application
import dev.tonyowen.jlr.network.JlrRepository
import dev.tonyowen.jlr.network.JlrService
import dev.tonyowen.jlr.ui.screens.CarDetailsViewModel
import dev.tonyowen.jlr.ui.screens.HomeScreenViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class JlrApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@JlrApplication)
            modules(appModule)
        }
    }
}

val appModule = module {
    single { JlrRepository(get()) }
    single { OkHttpClient() }
    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl("https://ksdjoqhkwoetfheugzix.supabase.co/rest/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    single { get<Retrofit>().create(JlrService::class.java) }

    viewModel { HomeScreenViewModel(get()) }
    viewModel { CarDetailsViewModel(get())}
}