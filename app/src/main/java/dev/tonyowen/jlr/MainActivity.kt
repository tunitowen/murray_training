package dev.tonyowen.jlr

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dev.tonyowen.jlr.ui.screens.CarDetailsScreen
import dev.tonyowen.jlr.ui.screens.HomeScreen
import dev.tonyowen.jlr.ui.theme.JLRTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {

            val navController = rememberNavController()

            JLRTheme {
                NavHost(navController = navController, startDestination = "home") {
                    composable("home") {
                        HomeScreen(navController = navController)
                    }
                    composable(route = "car_details/{id}",
                        arguments = listOf(navArgument("id") { type = NavType.IntType })) { backStackEntry ->
                        CarDetailsScreen(navController = navController, id = backStackEntry.arguments?.getInt("id"))
                    }
                }
            }
        }
    }
}