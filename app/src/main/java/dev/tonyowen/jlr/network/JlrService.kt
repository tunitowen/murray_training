package dev.tonyowen.jlr.network

import dev.tonyowen.jlr.network.model.Vehicle
import dev.tonyowen.jlr.network.model.VehicleDetails
import dev.tonyowen.jlr.network.model.VehicleDetailsBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.PATCH
import retrofit2.http.Query

const val API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImtzZGpvcWhrd29ldGZoZXVneml4Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTg2MDQ4NzIsImV4cCI6MjAxNDE4MDg3Mn0.SYacDOyyW2HnTAGULGbS4BzZHc0VozkaX9BUyXyo1EA"
interface JlrService {

    @Headers(
        "apikey: $API_KEY",
        "Authorization: $API_KEY"
    )
    @GET("v1/vehicles")
    suspend fun getVehicles(@Query("select") select: String = "*"): List<Vehicle>

    @Headers(
        "apikey: $API_KEY",
        "Authorization: $API_KEY"
    )
    @GET("v1/vehicle_details")
    suspend fun getVehicleDetails(@Query("select") select: String = "*", @Query("id") id: String): List<VehicleDetails>

    @Headers(
        "apikey: $API_KEY",
        "Authorization: $API_KEY"
    )
    @PATCH("v1/vehicle_details")
    suspend fun updateVehicleDetails(@Body body: VehicleDetailsBody, @Query("id") id: String): Response<Unit>
}