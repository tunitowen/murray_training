package dev.tonyowen.jlr.network.model

data class VehicleDetailsBody(
    val doors: Int,
    val windows: Int,
    val alarm: Int,
)