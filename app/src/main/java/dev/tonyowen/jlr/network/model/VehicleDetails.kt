package dev.tonyowen.jlr.network.model

data class VehicleDetails(
    val id: Int,
    val doors: Int,
    val windows: Int,
    val alarm: Int,
    val name: String,
    val image: String,
)

data class VehicleDeets(
    val id: Int,
    val doorsOpen: Boolean,
    val windowsOpen: Boolean,
    val alarmSet: Boolean,
    val name: String,
    val image: String,
)