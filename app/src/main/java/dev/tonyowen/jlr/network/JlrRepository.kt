package dev.tonyowen.jlr.network

import dev.tonyowen.jlr.network.model.Vehicle
import dev.tonyowen.jlr.network.model.VehicleDeets
import dev.tonyowen.jlr.network.model.VehicleDetails
import dev.tonyowen.jlr.network.model.VehicleDetailsBody
import retrofit2.Response

class JlrRepository(private val service: JlrService) {

    suspend fun getVehicles(): Result<List<Vehicle>> {
        return runCatching { service.getVehicles() }
    }

    suspend fun getVehicleDetails(id: Int): VehicleDeets {
        return service.getVehicleDetails(id = "eq.$id")
            .map {
                VehicleDeets(
                    id = it.id,
                    doorsOpen = it.doors.toBoolean(),
                    windowsOpen = it.windows.toBoolean(),
                    alarmSet = it.alarm.toBoolean(),
                    name = it.name,
                    image = it.image
                )
            }
            .first()
    }

    suspend fun updateVehicleDetails(deets: VehicleDeets): Result<Response<Unit>> {
        return runCatching {
            service.updateVehicleDetails(
                body = VehicleDetailsBody(
                    doors = deets.doorsOpen.toInt(),
                    windows = deets.windowsOpen.toInt(),
                    alarm = deets.alarmSet.toInt()
                ), id = "eq.${deets.id}"
            )
        }
    }
}

fun Int.toBoolean(): Boolean {
    return this == 1
}

fun Boolean.toInt(): Int {
    return if (this) 1 else 0
}