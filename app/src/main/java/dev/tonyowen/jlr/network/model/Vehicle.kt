package dev.tonyowen.jlr.network.model

data class Vehicle(
    val id: Int,
    val name: String,
    val image: String,
    val model: String,
)
