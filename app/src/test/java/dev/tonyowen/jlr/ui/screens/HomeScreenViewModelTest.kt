package dev.tonyowen.jlr.ui.screens

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import dev.tonyowen.jlr.network.JlrRepository
import dev.tonyowen.jlr.network.model.Vehicle
import io.mockk.MockKAnnotations.init
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HomeScreenViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: HomeScreenViewModel
    @RelaxedMockK
    private lateinit var repository: JlrRepository


    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        init(this)
        viewModel = HomeScreenViewModel(repository)
    }

    @Test
    fun `calling get vehicles calls repo`() {
        viewModel.getVehicles()
        coVerify { repository.getVehicles() }
    }
    @Test
    fun callingGetVehiclesSetsVehiclesValue() {
        coEvery { repository.getVehicles() } returns listOf(Vehicle(1, "name", "image", "model"))
        viewModel.getVehicles()
        assertTrue(viewModel.vehicles.value.size == 1)
        assertEquals(viewModel.vehicles.value[0].name, "name")
    }
}